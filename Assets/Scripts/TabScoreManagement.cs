﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using System;

public class TabScoreManagement : MonoBehaviour
{
    public GameObject txt_score_j;
    public GameObject txt_pseudos;
    public GameObject txt_scores;
    public GameObject txt_place_j;
    public GameObject txt_rule;
    public Button btn_valider;
    public Button btn_menu;
    public GameObject inp_pseudo;
    public GameObject popup;

    private ScoresArray sco = ScoresArray.init();

    // Start is called before the first frame update
    void Start()
    {
        btn_valider.onClick.AddListener(TaskOnClick);
        btn_menu.onClick.AddListener(GoMenu);

        if (GlobalVariables.SCORE == 0)
        {
            popup.SetActive(false);
            AffichageScore();
        }
    }

    void GoMenu() {
        SceneManager.LoadScene("Menu"); //, LoadSceneMode.Additive
    }

    void TaskOnClick() {
        InputField txt_input_pseudo = inp_pseudo.GetComponent<InputField>();
        string pseudoJoueur = txt_input_pseudo.text;

        if (pseudoJoueur == "") {
            txt_rule.SetActive(true);
        } else {
            popup.SetActive(false);

            //ScoresArray sco = ScoresArray.init();
            
            int scoreJoueur = GlobalVariables.SCORE;

            txt_score_j.GetComponent<Text>().text = "" + scoreJoueur;

            Scores joueur = new Scores(pseudoJoueur, scoreJoueur);

            sco.AddScore(joueur);
            txt_place_j.GetComponent<Text>().text = "" + sco.getPlace(joueur);
            AffichageScore();
        }
    }

    void AffichageScore() {
        string pseudos = "";
        string s = "";

        for (int i = 0; i < 10; i++)
        {
            if (i < sco.result.Length)
            {
                pseudos = pseudos + sco.result[i].id + "\n";
                s = s + sco.result[i].score + "\n";
            } else if (i >= sco.result.Length && i < 9) {
                pseudos = pseudos + "None\n";
                s = s + "?\n";
            } else {
                pseudos = pseudos + "None";
                s = s + "?";
            }
        }

        txt_pseudos.GetComponent<Text>().text = pseudos;
        txt_scores.GetComponent<Text>().text = s;
    }
}

[Serializable]
public class ScoresArray {
    public static string chemin = Application.streamingAssetsPath + "/score.json";
    public Scores[] result;

    public static ScoresArray init() {
        string jsonString = File.ReadAllText(chemin);
        ScoresArray sco = JsonUtility.FromJson<ScoresArray>(jsonString.ToString());
        return sco;
    }

    public void AddScore(Scores s) {
        Scores[] newR = new Scores[this.result.Length + 1];

        for (int i = 0; i < this.result.Length; i++) {
            newR[i] = this.result[i];
        }
        newR[newR.Length-1] = s;

        Array.Sort(newR, Scores.Compare);
        Array.Reverse(newR);

        this.result = newR;

        UpdateJson();
    }

    public int getPlace(Scores j) {
        int i = 0;
        bool find = false;
        while (!find)
        {
            if (j.id == this.result[i].id && j.score == this.result[i].score) {
                find = true;
            } else {
                i++;
            }
        }

        return i+1;
    }

    private void UpdateJson() {
        string scoreToJson = JsonUtility.ToJson(this, true);
        File.WriteAllText(chemin, scoreToJson);
    }
}

[Serializable]
public class Scores {
    public string id;
    public int score;

    public Scores(string id, int score) {
        this.id = id;
        this.score = score;
    }

    public static int Compare(Scores x, Scores y) {
        return x.score.CompareTo(y.score);
    }
}