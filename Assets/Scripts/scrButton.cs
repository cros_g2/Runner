﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scrButton : MonoBehaviour
{
	public void GoToScene(string sceneName)
	{
		FindObjectOfType<scrSoundManager>().Play("click");
		SceneManager.LoadScene(sceneName);
	}

	public void QuitGame()
	{
		FindObjectOfType<scrSoundManager>().Play("click");
		Application.Quit();
    }
}