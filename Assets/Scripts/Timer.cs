﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

public class Timer : MonoBehaviour
{
    public GameObject tSec;
    public float sec = 0;
    private bool start = false;

    // Start is called before the first frame update
    void Start()
    {
        setTimer(sec);
    }

    // Update is called once per frame
    void Update()
    {
        if (start)
        {
            sec += Time.deltaTime;
            tSec.GetComponent<Text>().text = ((int) sec).ToString();
        }
    }

    public void go(float s) {
        setTimer(s);
        start = true;
    }

    public void stop() {
        start = false;
    }

    public void setTimer(float s) {
        sec = s;
        tSec.GetComponent<Text>().text = ((int) sec).ToString();
    }
}
