﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scrPlayer : MonoBehaviour
{
    private float frame = 0;
    private float x = 0;
    private float y = 0;
    private float r = 2.5f; //radius
    private float speed = 0.6f; // between 0 and 1

    enum State {Running, Jumping, Sliding}
    State statePlayer;

    private float timeTillRunning = 0;
    private float jumpingTime = 0.4f;
    private float slidingTime = 0.4f;

    public GameObject blockToJump;
    public GameObject blockToSlide;
    [SerializeField] public GameObject[] specialBlocks;
    private int specialBlockChance;
    private int obstaclesThisLevel;
    private int obstaclesPassed;
    private int obstaclesMax;
    private Queue<GameObject> obstaclesList;
    private int visibleObstacles;

    public Animator animator;

    private int antiClockwise = 1; //1 = true, -1 = false
    private float xScale;

    // Start is called before the first frame update
    void Start()
    {
        GlobalVariables.SCORE = 0;

        specialBlockChance = 0; // between 0 and 100
        obstaclesMax = 8;
        obstaclesThisLevel = 4; // default: whatever
        obstaclesPassed = 2; // default: 1; Skips the first obstacle on the player's first position
        obstaclesList = new Queue<GameObject>(); // Initiates Obstacle History
        visibleObstacles = 3; // default: 3;  Max size of Obstacle History

        // Creates first Obstacle
        SpawnBlockAtRotation(4f / ((float)obstaclesThisLevel));
        SpawnBlockAtRotation(8f / ((float)obstaclesThisLevel));
        xScale = transform.localScale.x;
    }

    private void Update()
    {
        // Input
        if (statePlayer == State.Running)
        {
            if (Input.GetKeyDown("up"))
            {
                FindObjectOfType<scrSoundManager>().Play("jump");
                statePlayer = State.Jumping;
                timeTillRunning = jumpingTime;
                Debug.Log("Jumping");
                animator.SetBool("jumping", true);
            }
            if (Input.GetKeyDown("down"))
            {
                FindObjectOfType<scrSoundManager>().Play("slide");
                statePlayer = State.Sliding;
                timeTillRunning = slidingTime;
                Debug.Log("Sliding");
                animator.SetBool("sliding", true);
            }
        }

        // - will set the state to Running even without initializing it
        // - as of now, this variable isn't linked to the player's speed
        if (timeTillRunning > 0) timeTillRunning -= Time.deltaTime;
        else
        {
            if (statePlayer != State.Running)
            {
                statePlayer = State.Running;
                Debug.Log("Finished");
                if (animator.GetBool("jumping")) animator.SetBool("jumping", false);
                if (animator.GetBool("sliding")) animator.SetBool("sliding", false);
            }
        }
    }

    void FixedUpdate()
    {
        // Calculates the next position
        // With theses functions, a complete turn is when "frame == 4"
        frame += speed * Time.deltaTime;
            x = r * Mathf.Cos(Mathf.PI / 2 * (frame - 1)) * antiClockwise;
            y = r * Mathf.Sin(Mathf.PI / 2 * (frame - 1));

        // Moves and Rotates the Player
        transform.position = new Vector3(x, y, 0);
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, frame / 4f * 360f * antiClockwise));

        transform.localScale = new Vector3(xScale * antiClockwise, transform.localScale.y, transform.localScale.z);


        /*
        switch (statePlayer)
        {
            case State.Running:
                gameObject.GetComponent<Animator>().Play("Run");
                break;
            case State.Jumping:
                gameObject.GetComponent<Animator>().Play("Jump");
                break;
            case State.Sliding:
                gameObject.GetComponent<Animator>().Play("Slide");
                break;
            default:
                break;
        }*/
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Did the player dodge correctly?
        if ((collision.tag == "ToJump" && statePlayer == State.Jumping)
        || (collision.tag == "ToSlide" && statePlayer == State.Sliding))
        {
            Debug.Log("OK");
            GlobalVariables.SCORE++;
        }
        else
        {
            FindObjectOfType<scrSoundManager>().Play("death");
            Debug.Log("GameOver");
            SceneManager.LoadScene("EndGameScene");
        }

        // Calculates next obstacle position
        obstaclesPassed++;
        if (obstaclesPassed == obstaclesThisLevel)
        {
            if (obstaclesThisLevel < obstaclesMax) obstaclesThisLevel++;
            obstaclesPassed = 0;
        }
        float obsPos = (((float)obstaclesPassed) / ((float)obstaclesThisLevel)) * 4f;
        SpawnBlockAtRotation(obsPos);

    }

    private void addToObstaclesList(GameObject o)
    {
        // Adds Obstacle to the history, removes the one too far behind
        obstaclesList.Enqueue(o);
        if (obstaclesList.Count > visibleObstacles) Destroy(obstaclesList.Dequeue());
    }


    private void SpawnBlockAtRotation(float rot) //between 0 and 4
    {
        // Generates an obstacle
        int chance = Random.Range(0, 100);
        // Is it special?
        GameObject obstacle = null;
        if (chance < specialBlockChance)
        {
            // Overwrites as a Special Block
            obstacle = Instantiate(specialBlocks[Random.Range(0, specialBlocks.Length)]);
            // As of now: this line chooses a special block randomly from every special blocks
        } else
        {
            // If not, what is it? Slide or Jump?
            chance = Random.Range(0, 100);
            if (chance < 50)
            {
                obstacle = Instantiate(blockToSlide);
            } else
            {
                obstacle = Instantiate(blockToJump);
            }
            // This block was made like this to prevent issues with the Instantiate Function (can't "overwrite" a block)
        }

        var frame = rot;
        var x = r * Mathf.Cos(Mathf.PI / 2 * (frame - 1));
        var y = r * Mathf.Sin(Mathf.PI / 2 * (frame - 1));

        obstacle.transform.position = new Vector3(x, y, 0);
        obstacle.transform.rotation = Quaternion.Euler(new Vector3(0, 0, frame / 4f * 360f));

        // Adds it to the obstacle history
        addToObstaclesList(obstacle);
    }
}
